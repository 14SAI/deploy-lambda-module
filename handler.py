import json

def karthi(event, context):

    body = {
        "message": "executed successfully!",
        "input": event
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return response
